package com.example.john.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

@SuppressLint("NewApi")
public class MainActivity extends AppCompatActivity {
    protected static final String TAG = "main";
    private Camera mCamera;
    private CameraPreview mPreview;
    public File pictureFile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCamera = getCameraInstance();

        // 创建预览类，并与Camera关联，最后添加到界面布局中
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);

        preview.addView(mPreview);

        Button captureButton = (Button) findViewById(R.id.button_capture);  //button_capture

        captureButton.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                Camera.Parameters parameters = mCamera.getParameters();
                // 设置相片格式
                parameters.setJpegQuality(100);  //生成图像的质量
                mCamera.setParameters(parameters);
                parameters.setPictureFormat(ImageFormat.JPEG);
                // 设置预览大小
                parameters.setPreviewSize(800, 480);
                parameters.setPictureSize(4160, 3120); //生成图像的风辨率
                mCamera.setParameters(parameters);
                parameters.setRotation(90);  //生成的图像旋转90度
                mCamera.setParameters(parameters);
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(parameters);
                mCamera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        // 从Camera捕获图片
                        mCamera.takePicture(null, null, mPicture);
                    }
                });
                /*if (Cur_State < 2) {
                    switch (Cur_State) {
                        case 0:
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                            mCamera.setParameters(parameters);
                            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                                @Override
                                public void onAutoFocus(boolean success, Camera camera) {
                                    // 从Camera捕获图片
                                    mCamera.takePicture(null, null, mPicture);

                                }
                            });
                            Cur_State++;
                            break;
                        case 1:
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);//FLASH_MODE_OFF
                            mCamera.setParameters(parameters);
                            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                                @Override
                                public void onAutoFocus(boolean success, Camera camera) {
                                    // 从Camera捕获图片
                                    mCamera.takePicture(null, null, mPicture);

                                }
                            });
                            Cur_State++;
                            break;
                    }
                } else {
                    Cur_State = 0;
                }*/

            }
        });
    }
    /** 检测设备是否存在Camera硬件 */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // 存在
            return true;
        } else {
            // 不存在
            return false;
        }
    }
    /** 打开一个Camera */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
            Log.d(TAG, "打开Camera失败失败");
        }
        return c;
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera arg1) {
            //将拍照得到的数据信息存储到本地
            File tempFile=new File("/sdcard/temp.png");
            try {
                FileOutputStream fos=new FileOutputStream(tempFile);
                fos.write(data);
                fos.close();

                //注意，，，然后将这个照片的数据信息传送给要进行展示的Activity即可，这部
                //分代码是我自己需要的，你使用时需要将他去掉修改成你的。
                Intent intent=new Intent(MainActivity.this,MainActivity.class);
                intent.putExtra("PicturePath", tempFile.getAbsolutePath());
                startActivity(intent);
                //拍照结束之后销毁当前的Activity，进入到图片展示界面
                MainActivity.this.finish();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onDestroy() {
        // 回收Camera资源
        if(mCamera!=null){
            mCamera.stopPreview();
            mCamera.release();
            mCamera=null;
        }
        super.onDestroy();
    }
}
